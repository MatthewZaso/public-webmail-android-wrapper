package com.mattzaso.publicis.publiciswebemail;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebViewActivity extends ActionBarActivity {

  private WebView mainWebView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_web_view);

    mainWebView = (WebView) findViewById(R.id.main_web_view);

    WebSettings webSettings = mainWebView.getSettings();
    webSettings.setJavaScriptEnabled(true);
    webSettings.setUserAgentString("Mozilla/5.0 (Linux; 4.4.4; en-us; Nexus 5 Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36");

    mainWebView.loadUrl("https://mail.publicisgroupe.net");

  }

}
